function createCard(name, description, pictureUrl, created, ends, location) {
    return `
    <div class="col mb-3">
      <div class="card" style = "box-shadow: 0px 5px 5px 3px gray;  >
        <img src="${pictureUrl}" class="card-img-top">
        <div class ="card-header" style = "color: gray">${location}</div>
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <p class="card-text">${description}</p>
        </div>
        <div class="card-footer">${created} - ${ends}

      </div>
    </div>
    `;
  }
window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/';
    try{
        const response = await fetch(url);

        if (!response.ok) {
            return window.alert("Bad Response")
        } else{
            const data = await response.json();


            for (let conference of data.conferences){
                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);

                if (detailResponse.ok) {
                    const details = await detailResponse.json();

                    const title = details.conference.name;
                    const location = details.conference.location.name
                    const description = details.conference.description;
                    const pictureUrl = details.conference.location.picture_url;
                    const created = Date(details.conference.created)
                    const ends = Date(details.conference.ends)
                    const html = createCard(title, description, pictureUrl, created, ends, location);
                    const column = document.querySelector('.col');
                    column.innerHTML += html;
                }
            }
        }
    } catch (e) {

        return window.alert("Fetch Error")
    }
});
